﻿using KanbanApi.Authorization;
using KanbanApi.Data;
using KanbanApi.Encryption;
using KanbanApi.Requests;
using KanbanApi.Responses;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace KanbanApi.Controllers
{
    [ApiController]
    [Route("login")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly KanbanContext _context;
        private readonly IPasswordEncryption _passwordEncryption;
        public AuthenticationController(
            IConfiguration configuration,
            KanbanContext context, 
            IPasswordEncryption passwordEncryption)
        {
            _configuration = configuration;
            _context = context;
            _passwordEncryption = passwordEncryption;
        }

        [HttpPost]
        public async Task<ActionResult<ErrorResponseBase>> Post([FromBody] LoginRequest request)
        {
            var user = await _context.Users.Where(u => u.Login == request.Login).FirstOrDefaultAsync();

            if (user == null)
            {
                return BadRequest(new InvalidCredentialsResponse());
            }

            if (!_passwordEncryption.VerifyPassword(user.Password, request.Password))
            {
                return BadRequest(new InvalidCredentialsResponse());
            }

            var token = TokenFactory.GenerateToken(_configuration["TokenSecret"], user);

            return Ok(token);
        }
    }
}
