﻿using KanbanApi.Data;
using KanbanApi.Encryption;
using KanbanApi.Models;
using KanbanApi.Requests;
using KanbanApi.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace KanbanApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class CardsController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly KanbanContext _context;
        private readonly IPasswordEncryption _passwordEncryption;

        public CardsController(
            ILogger<CardsController> logger,
            KanbanContext context,
            IPasswordEncryption passwordEncryption)
        {
            _logger = logger;
            _context = context;
            _passwordEncryption = passwordEncryption;
        }

        [HttpGet]
        public async Task<IEnumerable<Card>> GetAll()
        {
            return await _context.Cards.ToListAsync();
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<Card>> GetById(Guid id)
        {
            var card =  await _context.Cards.FindAsync(id);

            if (card == null)
            {
                return NotFound(new ErrorResponseBase("No card was found for the 'ID' provided."));
            }

            return Ok(card);
        }

        [HttpPost]
        public async Task<ActionResult<Card>> CreateCard([FromBody] CreateOrUpdateCardRequest request)
        {
            var errors = request.IsValid();

            if (errors.Any())
            {
                return BadRequest(new AttributesValidationErrorsResponse(
                    "One or more attributes have invalid values.", errors));
            }

            var card = new Card(request.Title, request.Content, request.List);

            await _context.Cards.AddAsync(card);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetById), new { id = card.Id }, card);
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<ActionResult<Card>> UpdateCard([FromRoute] Guid id, [FromBody] CreateOrUpdateCardRequest request)
        {
            var errors = request.IsValid();

            if (errors.Any())
            {
                return BadRequest(new AttributesValidationErrorsResponse(
                    "One or more attributes have invalid values.", errors));
            }

            var card = await _context.Cards.FindAsync(id);

            if (card == null)
            {
                return NotFound(new ErrorResponseBase("No card was found for the 'ID' provided."));
            }

            _logger.LogInformation($"{DateTime.Now.ToString("G", CultureInfo.GetCultureInfo("pt-BR"))} - " +
                $"Card {card.Id} - {card.Title} - Alterado");

            card.Title = request.Title;
            card.Content = request.Content;
            card.List = request.List;

            await _context.SaveChangesAsync();

            return Ok(card);
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<ActionResult<List<Card>>> DeleteCard([FromRoute] Guid id)
        {
            var card = await _context.Cards.FindAsync(id);

            if (card == null)
            {
                return NotFound(new ErrorResponseBase("No card was found for the 'ID' provided."));
            }

            _context.Cards.Remove(card);
            await _context.SaveChangesAsync();

            _logger.LogInformation($"{DateTime.Now.ToString("G", CultureInfo.GetCultureInfo("pt-BR"))} - " +
                $"Card {card.Id} - {card.Title} - Removido");

            var cardsList = await _context.Cards.ToListAsync();

            return Ok(cardsList);
        }
    }
}
