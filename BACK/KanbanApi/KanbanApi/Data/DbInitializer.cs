﻿using KanbanApi.Encryption;
using KanbanApi.Models;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace KanbanApi.Data
{
    public class DbInitializer
    {
        public static void Initialize(IConfiguration configuration, KanbanContext context, IPasswordEncryption passwordEncryption)
        {
            context.Database.EnsureCreated();

            if (context.Users.Any())
            {
                return;
            }

            var defaultUserLogin = configuration["ApiDefaultUser:Login"];
            var defaultUserHashedPassword = passwordEncryption.HashPassword(configuration["ApiDefaultUser:Password"]);

            var defaultUser = new User(defaultUserLogin, defaultUserLogin, defaultUserHashedPassword);

            context.Users.Add(defaultUser);

            context.SaveChanges();
        }
    }
}
