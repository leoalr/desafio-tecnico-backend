﻿using KanbanApi.Models;
using Microsoft.EntityFrameworkCore;

namespace KanbanApi.Data
{
    public class KanbanContext : DbContext
    {
        public KanbanContext(DbContextOptions<KanbanContext> options)
            : base (options)
            { }

        public DbSet<User> Users { get; set; }
        public DbSet<Card> Cards { get; set; }
    }
}
