﻿namespace KanbanApi.Encryption
{
    public interface IPasswordEncryption
    {
        string HashPassword(string password);

        bool VerifyPassword(string hashedPassword, string submittedPassword);
    }
}
