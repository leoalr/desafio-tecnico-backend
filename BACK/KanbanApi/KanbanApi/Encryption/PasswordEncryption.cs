﻿namespace KanbanApi.Encryption
{
    public class PasswordEncryption : IPasswordEncryption
    {
        public string HashPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }

        public bool VerifyPassword(string hashedPassword, string submittedPassword)
        {
            return BCrypt.Net.BCrypt.Verify(submittedPassword, hashedPassword);
        }
    }
}
