﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace KanbanApi.Models
{
    public class Card
    {
        public Card()
        {
            Id = Guid.NewGuid();
        }

        public Card(string title, string content, string list)
        {
            Id = Guid.NewGuid();
            Title = title;
            Content = content;
            List = list;
        }

        [JsonPropertyName("id")]
        public Guid Id { get; set; }
        [JsonPropertyName("titulo")]
        public string Title { get; set; }
        [JsonPropertyName("conteudo")]
        public string Content { get; set; }
        [JsonPropertyName("lista")]
        public string List { get; set; }

    }
}
