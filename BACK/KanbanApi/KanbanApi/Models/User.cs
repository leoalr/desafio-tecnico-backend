﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace KanbanApi.Models
{
    public class User
    {
        public User()
        {
            Id = Guid.NewGuid();
        }

        public User(string name, string login, string password)
        {
            Id = Guid.NewGuid();
            Name = name;
            Login = login;
            Password = password;
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
