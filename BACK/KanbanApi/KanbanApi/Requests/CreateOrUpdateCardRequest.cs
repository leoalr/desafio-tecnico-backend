﻿using KanbanApi.Responses;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace KanbanApi.Requests
{
    public class CreateOrUpdateCardRequest
    {
        [JsonPropertyName("titulo")]
        public string Title { get; set; }

        [JsonPropertyName("conteudo")]
        public string Content { get; set; }

        [JsonPropertyName("lista")]
        public string List { get; set; }

        public List<AttributeValidationError> IsValid()
        {
            var attributeErrors = new List<AttributeValidationError>();

            if (string.IsNullOrEmpty(Title))
            {
                attributeErrors.Add(new AttributeValidationError()
                {
                    AttributeName = "titulo",
                    ValidationErrorMessage = "The attribute 'titulo' must have a valid string value."
                });
            }

            if (string.IsNullOrEmpty(Content))
            {
                attributeErrors.Add(new AttributeValidationError()
                {
                    AttributeName = "conteudo",
                    ValidationErrorMessage = "The attribute 'conteudo' must have a valid string value."
                });
            }

            if (string.IsNullOrEmpty(List))
            {
                attributeErrors.Add(new AttributeValidationError()
                {
                    AttributeName = "lista",
                    ValidationErrorMessage = "The attribute 'lista' must have a valid string value."
                });
            }

            return attributeErrors;
        }
    }
}
