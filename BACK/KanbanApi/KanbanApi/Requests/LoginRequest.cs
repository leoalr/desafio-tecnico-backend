﻿using System.Text.Json.Serialization;

namespace KanbanApi.Requests
{
    public class LoginRequest
    {
        public string Login { get; set; }

        [JsonPropertyName("senha")]
        public string Password { get; set; }
    }
}
