﻿namespace KanbanApi.Responses
{
    public class AttributeValidationError
    {
        public AttributeValidationError() { }

        public AttributeValidationError(string attributeName, string validationErrorMessage)
        {
            AttributeName = attributeName;
            ValidationErrorMessage = validationErrorMessage;
        }

        public string AttributeName { get; set; }
        public string ValidationErrorMessage { get; set; }
    }
}
