﻿using System.Collections.Generic;

namespace KanbanApi.Responses
{
    public class AttributesValidationErrorsResponse : ErrorResponseBase
    {
        public AttributesValidationErrorsResponse(string message)
            : base(message) { }

        public AttributesValidationErrorsResponse(string message, List<AttributeValidationError> errors)
            : base(message)
        {
            ValidationErrors = errors;
        }

        public List<AttributeValidationError> ValidationErrors { get; set; }
    }
}
