﻿namespace KanbanApi.Responses
{
    public class ErrorResponseBase
    {
        public ErrorResponseBase() { }
        public ErrorResponseBase(string message)
        {
            Message = message;
        }
        public string Message { get; set; }
    }
}
