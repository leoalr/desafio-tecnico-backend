﻿namespace KanbanApi.Responses
{
    public class InvalidCredentialsResponse : ErrorResponseBase
    {
        public InvalidCredentialsResponse()
            : base("Invalid login or password.") { }
    }
}
