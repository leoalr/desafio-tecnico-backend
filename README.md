# Como executar a solução do desafio

## Para executar a solução deste desafio precisaremos:

1) Ter o Docker Compose instalado na máquina
2) Clonar o repositório
3) Configurar o segredo e credenciais de acesso à API conforme explicado abaixo.

## Para configurar a API da solução, após clonar este repositório, deveremos proceder da seguinte forma:

1) Alterar os arquivos de configuração da API.  
Para isso, no diretório local do repositório que acaba de ser clonado, deverá acessar o caminho ```BACK\KanbanApi\KanbanApi```, abrir o arquivo ```appsettings.example.json``` e no objeto JSON do conteúdo deste arquivo alterar os valores das três configurações conforme a seguir:   

    a) Para a propriedade "TokenSecret" atribuir um valor ```string``` que preferir, como por exemplo **"minhachave123456"**, ou **"meusegredo123456"**, substituindo o valor original **"\<seusegredo>"**.  
    **OBS. IMPORTANTE:** O valor desse segredo deve possuir **NO MÍNIMO** 16 caracteres como os exemplos acima. Esta é uma exigência do algoritmo de criptografia utilizado pela solução.

    b) No objeto que está atribuído à propriedade **"ApiDefaultUser"**, alterar a propriedade **"Login"** para atribuir como valor o login de usuário que deseja utilizar no client (front-end) para acessar a API.  
    **Obs.:** Atualmente o client (front-end) está utilizando o login **"letscode"**.  

    c) No objeto que está atribuído à propriedade **"ApiDefaultUser"**, alterar a propriedade **"Password"** para atribuir como valor a senha de usuário que deseja utilizar no client (front-end) para acessar a API.  
    **Obs.:** Atualmente o client (front-end) está utilizando a senha **"lets@123"**.

    Desta forma, o conteúdo do arquivo appsettings.example.json deverá ficar por exemplo, assim:

    ```json
    {
        "TokenSecret": "meusegredo1234muitoseguro",
        "ApiDefaultUser": {
            "Login": "letscode",
            "Password": "lets@123"
        },
        "Logging": {
            "LogLevel": {
            "Default": "Information",
            "Microsoft": "Warning",
            "Microsoft.Hosting.Lifetime": "Information"
            }
        },
        "AllowedHosts": "*"
    }
    ```

2) Alterar os nomes dos arquivos ```appsettings.example.json``` e ```appsettings.example.Development.json``` removendo de cada um, o trecho **```".example"```**, de forma que os nomes desses arquivos passem a ser, respectivamente, ```appsettings.json``` e ```appsettings.Development.json```.

## **Pronto!! A Api já está configurada! =)**  


Agora, para executar a solução, basta abrir um terminal de comando, ir ao diretório raiz do repositório, que caso não tenha alterado o default, se chamará **```"deasfio-tecnico-backend"```**, e executar o comando:  
```
> docker-compose up
```

Se Murphy não estiver atuante no momento que você executar este comando, não deverá ocorrer nenhum erro, e portanto para acessar a aplicação deverá através de um browser ir ao endereço:  
http://localhost:3000/  

Lembrando que a API estará funcionando através do endereço:  
http://localhost:5000/  

Caso queira testar a API separadamente, poderá, se quiser utilizar a collection de requests do Insomnia, que está em:
```
\desafio-tecnico-backend\BACK\Insomnia_2022-04-11.json
```
E caso queira ver o swagger da API, para efeito de documentação, poderá acessar através de um browser, o endereço:  
http://localhost:5000/swagger  


---
### **Comentários do candidato aos avaliadores da solução do desafio:**

Só conseguir fazer o docker-compose funcionar removendo uma configuração (```"react-app/jest"```) do ```eslint``` do front, que estava no ```package.json```, dentro de ```eslintConfig``` >> ```extends```.  

Provavelmente deve ter sido alguma incompatibilidade entre o ```eslint``` e a versão do ```node / yarn``` que utilizei na imagem do Docker. Como não tive muito tempo para investigar isso mais a fundo, fiquei com essa solução mesmo de alterar essa propriedade no package.json do FRONT, o que não considero o ideal.

Lembrando que fora do docker, na minha máquina, o FRONT funcionou sem precisar remover essa propriedade, por isso fiquei um pouco chateado de não encontrar melhor solução no pouco tempo que tentei.

Ahhh... outra coisa, eu adoro testes unitários e tenho boa experiência com eles, mas me enrolei um pouco e não tive tempo de os fazer, preferi priorizar o docker-compose.

Espero que compreendam! =)

--- 

-----
## ###### ABAIXO O README ORIGINAL ###### ## 

-----

# Desafio Técnico - Backend

O propósito desse desafio é a criação de uma API que fará a persistência de dados de um quadro de kanban. Esse quadro possui listas, que contém cards.

## Rodando o Frontend

Um frontend de exemplo foi disponibilizado na pasta FRONT.

Para rodá-lo, faça:

```console
> cd FRONT
> yarn
> yarn start
```

## Desafio

Você precisa criar uma API REST de acordo com os requisitos abaixo, que deve ser desenvolvido na pasta "BACK".

Para criar sua API você pode escolher entre duas tecnologias:

1. Javascript ou Typescript + NodeJS + Express
2. C# + ASP.NET Core + WebApi

## Requisitos

1. O sistema deve ter um mecanismo de login usando JWT, com um entrypoint que recebe `{ "login":"letscode", "senha":"lets@123"}` e gera um token.

2. O sistema deve ter um middleware que valide se o token é correto, valido e não está expirado, antes de permitir acesso a qualquer outro entrypoint. Em caso negativo retorne status 401.

3. O login e senha fornecidos devem estar em variáveis de ambiente e terem uma versão para o ambiente de desenvolvimento vinda de um arquivo .env no node ou de um arquivo de configuração no ASP.NET. Esse arquivo não deve subir ao GIT, mas sim um arquivo de exemplo sem os valores reais. O mesmo vale para qualquer "segredo" do sistema, como a chave do JWT.

4. Um card terá o seguinte formato: 

```
id: int | (guid [c#] | uuid [node])
titulo : string, 
conteudo: string, 
lista: string
```

5. Os entrypoints da aplicação devem usar a porta 5000 e ser:

```
(POST)      http://0.0.0.0:5000/login/

(GET)       http://0.0.0.0:5000/cards/
(POST)      http://0.0.0.0:5000/cards/
(PUT)       http://0.0.0.0:5000/cards/{id}
(DELETE)    http://0.0.0.0:5000/cards/{id}
```

6. Para inserir um card o título, o conteúdo e o nome da lista devem estar preenchidos, o id não deve conter valor. Ao inserir retorne o card completo incluindo o id atribuído com o statusCode apropriado. Caso inválido, retorne status 400.

7. Para alterar um card, o entrypoint deve receber um id pela URL e um card pelo corpo da requisição. Valem as mesmas regras de validação do item acima exceto que o id do card deve ser o mesmo id passado pela URL. Na alteração todos os campos são alterados. Caso inválido, retorne status 400. Caso o id não exista retorne 404. Se tudo correu bem, retorne o card alterado.

8. Para remover um card, o entrypoint deve receber um id pela URL. Caso o id não exista retorne 404. Se a remoção for bem sucedida retorne a lista de cards.

9. A listagem de cards deve enviar todos os cards em formato json, contendo as informações completas. 

10. Deve ser usada alguma forma de persistência, no C# pode-se usar o Entity Framework (in-memory), no nodeJS pode ser usado Sequelize + sqlite (in-memory) ou diretamente o driver do sqlite (in-memory).

11. Se preferir optar por utilizar um banco de dados "real", adicione um docker-compose em seu repositório que coloque a aplicação e o banco em execução, quando executado `docker-compose up` na raiz. A connection string e a senha do banco devem ser setados por ENV nesse arquivo.

12. O campo conteúdo do card aceitará markdown, isso não deve impactar no backend, mas não custa avisar...

13. Faça um filter (asp.net) ou middleware (nodejs) que escreva no console sempre que os entrypoints de alteração ou remoção forem usados, indicando o horário formatado como o datetime a seguir: `01/01/2021 13:45:00`. 

A linha de log deve ter o seguinte formato (se a requisição for válida):

`<datetime> - Card <id> - <titulo> - <Remover|Alterar>`

Exemplo:

```console
> 01/01/2021 13:45:00 - Card 1 - Comprar Pão - Removido
```

14. O projeto deve ser colocado em um repositório GITHUB ou equivalente, estar público, e conter um readme.md que explique em detalhes qualquer comando ou configuração necessária para fazer o projeto rodar. Por exemplo, como configurar as variáveis de ambiente, como rodar migrations (se foram usadas). 

15. A entrega será apenas a URL para clonarmos o repositório.

## Diferenciais e critérios de avaliação

Arquiteturas que separem responsabilidades, de baixo acoplamento e alta-coesão são preferíveis, sobretudo usando dependências injetadas, que permitam maior facilidade para testes unitários e de integração.

Avaliaremos se o código é limpo (com boa nomenclatura de classes, variáveis, métodos e funções) e dividido em arquivos bem nomeados, de forma coesa e de acordo com boas práticas. Bem como práticas básicas como tratamento de erros.

Desacoplar e testar as regras de negócios / validações / repositório com testes unitários será considerado um diferencial.

O uso de typescript no node acompanhado das devidas configurações e tipagens bem feitas, bem como uso de técnicas de abstração usando interfaces (especialmente do repositório) serão consideradas um deferencial.

O uso de Linter será considerado um diferencial.

A criação de um docker-compose e de dockerfiles que ao rodar `docker-compose up` subam o sistema por completo (front, back e banco [se houver]) será considerado um diferencial.

Teve dificuldade com algo, ou fez algo meio esquisito para simplificar algo que não estava conseguindo fazer? Deixe uma observação com a justificativa no readme.md para nós...

Entregou incompleto, teve dificuldade com algo, ou fez algo meio esquisito para simplificar alguma coisa que não estava conseguindo fazer? Deixe uma observação com a justificativa no readme.md para nós...